---
title: Workflow
# for Jekyll
layout: page
# for Pandoc
markdown:
  flavor: markdown
...

NOTE: This page uses the
  [PP](http://cdsoft.fr/pp/) Markdown pre-processor
  and [Pandoc's Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) syntax.
  There is also a [PP usage tutorial](https://github.com/tajmone/markdown-guide/tree/master/pp).

## PDF Generation

Tree/Network structured Markdown sources get mapped to a linear structure,
for example a PDF document.

!dot([!gensrcdir/]!projdir/assets/images/tree_linearization_source_dot.svg { width=74% })
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/tree_linearization_sources.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!dot([!gensrcdir/]!projdir/assets/images/tree_linearization_linear_dot.svg { width=25% })
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/tree_linearization_linear.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Options

!dot([!gensrcdir/]!projdir/assets/images/workflow_dot.svg { width=96% })
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/workflow.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
