---
title: tests
# for Jekyll
layout: page
# for Pandoc
markdown:
  flavor: markdown
---

## Pandoc

Resized image:

![](!projdir/assets/images/graph_inline_helloWorld.dot.svg){#id .class width=120 height=50px}

### Layouting

#### Using image attributes

!dot([!gensrcdir/]!projdir/assets/images/tree_linearization_source_dot.svg { width=74% })
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/tree_linearization_sources.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!dot([!gensrcdir/]!projdir/assets/images/tree_linearization_linear_dot.svg { width=25% })
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/tree_linearization_linear.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#### Using an HTML table

##### PP generated images

NOTE: This does not work, because markdown is not parsed within HTML!

<table>
<tr>
<td>

!dot([!gensrcdir/]!projdir/assets/images/tree_linearization_source_dot.svg)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/tree_linearization_sources.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

</td>
<td>

!dot([!gensrcdir/]!projdir/assets/images/tree_linearization_linear_dot.svg)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/tree_linearization_linear.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

</td>
</tr>
</table>

##### Non-generated, HTML referenced images

<table>
<tr>
<td>
<img src="!projdir/assets/images/tree_linearization_source_dot.svg"/>
</td>
<td>
<img src="!projdir/assets/images/tree_linearization_linear_dot.svg"/>
</td>
</tr>
</table>

Only one of these works; please test which one
(it is supposed to link to the next section heading):

* [auto-gen ref "md-pp"](#md-pp)
* [custom ref "t-md-pp" (Pandoc)](#t-md-pp)
* [auto-gen ref "6-9-md-pp" (_unlikely_)](#6-9-md-pp)
* [auto-gen ref "69-md-pp" (_unlikely_)](#69-md-pp)
* [auto-gen ref "69-md-pp-t-md-pp" (GitLab)](#69-md-pp-t-md-pp)

## 6.9 MD-PP {#t-md-pp}

### Graphs

A Dot graph, in-line:

!dot([!gensrcdir/]!projdir/assets/images/graph_inline_helloWorld.dot.svg)(My optional legend)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    graph {
        "source code of the diagram"
    }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A dot graph, from external file:

!dot([!gensrcdir/]!projdir/assets/images/tree_linearization_sources.dot.svg)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
!include(!cwd/!projdir/assets/diagrams/tree_linearization_sources.dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

---

Much more of PP's capabilities can be seen [here](39_pp.pp.md).
