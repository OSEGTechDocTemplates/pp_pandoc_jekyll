# OSEG DIN Guideline - Documentation Example - Pandoc - 3 - Pre-Processor, Pandoc (PDF) & Jekyll

Result:
[HTML](https://osegtechdoctemplates.gitlab.io/pp_pandoc_jekyll/) |
[PDF](https://osegtechdoctemplates.gitlab.io/pp_pandoc_jekyll/doc.pdf) |
[Combined Markdown](https://osegtechdoctemplates.gitlab.io/pp_pandoc_jekyll/doc.md)

This contains just the most simple possible way:

* [ ] a single Markdown source file
* [x] PP (pre-processor) directives
* [x] pandoc
* [x] jekyll
* [ ] pdsite
* [ ] sphinx
* [x] output: HTML
* [x] output: PDF
